/*
 * Finding the maximum matching in the given bipartite graph using
 * Hopcroft-Karp algorithm.
 */

#include "rangi.h"

#define VNIL		(MNODES - 1)	/* the special null vertex */
#define INF		1000000

static int bfs(int **adj, int *pair, int *pair2, int *dist, int n1)
{
	int q[MNODES];
	int n = 0;
	int i;
	for (i = 0; i < n1; i++) {
		if (adj[i][0] >= 0 && pair[i] == VNIL) {
			dist[i] = 0;
			q[n++] = i;
		} else {
			dist[i] = INF;
		}
	}
	dist[VNIL] = INF;
	while (n) {
		int v = q[--n];
		for (i = 0; adj[v][i] >= 0; i++) {
			int u = adj[v][i];
			if (dist[pair2[u]] == INF) {
				dist[pair2[u]] = dist[v] + 1;
				q[n++] = pair2[u];
			}
		}
	}
	return dist[VNIL] != INF;
}

static int dfs(int **adj, int *pair, int *pair2, int *dist, int v)
{
	int i;
	if (v == VNIL)
		return 1;
	for (i = 0; adj[v][i] >= 0; i++) {
		int u = adj[v][i];
		if (dist[pair2[u]] == dist[v] + 1) {
			if (dfs(adj, pair, pair2, dist, pair2[u])) {
				pair2[u] = v;
				pair[v] = u;
				return 1;
			}
		}
	}
	dist[v] = INF;
	return 0;
}

/*
 * Find the maximum matching in the given bipartite graph.  adj
 * is the adjacency list of the first partition.
 */
int matching(int **adj, int n1, int n2)
{
	int i;
	int mat = 0;
	int dist[MNODES];
	int pair[MPARTS];	/* vertices matched to the first partition */
	int pair2[MPARTS];	/* vertices matched to the second partition */
	int noadj[] = {-1};
	for (i = 0; i < n1; i++)
		pair[i] = VNIL;
	for (i = 0; i < n2; i++)
		pair2[i] = VNIL;
	adj[VNIL] = noadj;
	while (bfs(adj, pair, pair2, dist, n1))
		for (i = 0; i < n1; i++)
			if (pair[i] == VNIL && dfs(adj, pair, pair2, dist, i))
				mat++;
	return mat;
}
