#define NTHREADS	64
/* for the input graph */
#define NNODES		1000
#define NSOLS		10000
#define NCOLORS		512
#define NLEN		64
/* for the vertex-color bipartite graph */
#define MPARTS		NCOLORS			/* nodes in each partition */
#define MNODES		(MPARTS * 2 + 2)	/* total nodes */

int matching(int **adj, int n1, int n2);
void proc_init(int beg, int end, int inc, int one, int doheur);
void found(int *nodes);
int ownroot(int nsub, int idx);
int motifs(int *subs[], int size);
int nsub_next(int last);
int nsub_finished(int nsub);
void nsub_detected(int last);
