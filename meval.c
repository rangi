/*
 * meval, count the complexes containing a motif
 *
 * Copyright (C) 2012 Ali Gholami Rudi <ali at rudi dot ir>
 *
 * This program is released under the modified BSD license.
 */
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define NPROTS		(1024)
#define NLEN		(128)

static int readcmplx(char cmplx[][NLEN], char *s)
{
	int n = 0;
	while (*s && *s != '\t')
		s++;
	while (*s) {
		char *d = cmplx[n++];
		while (*s && !isspace(*s))
			*d++ = *s++;
		*d = '\0';
		while (*s && isspace(*s))
			s++;
	}
	return n;
}

int main(int argc, char *argv[])
{
	char cmplx[NPROTS][NLEN];
	char **query = argv + 1;
	int qlen = argc - 1;
	char line[4096];
	int i, j;
	int matches = 0;
	if (!qlen) {
		printf("usage: %s PROT1 PROT2 ... <complexes\n", argv[0]);
		return 1;
	}
	while (fgets(line, sizeof(line), stdin)) {
		int clen = readcmplx(cmplx, line);
		for (i = 0; i < qlen; i++) {
			for (j = 0; j < clen; j++)
				if (!strcmp(query[i], cmplx[j]))
					break;
			if (j == clen)
				break;
		}
		if (i == qlen)
			matches++;
	}
	printf("%d\n", matches);
	return 0;
}
