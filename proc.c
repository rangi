/* handling solutions and root divisions */
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include "rangi.h"

static int nsub_beg;		/* the smallest motif size to search for */
static int nsub_end;		/* the largest motif size to search for */
static int nsub_inc;		/* the direction of exploring motif sizes */
static int one;			/* exit after the first motif */
static int dodetect;		/* early motif detection heuristic */

static pthread_mutex_t lock;	/* for accessing the following variables */
static int lastroot = -1;	/* the last owned root */
static int lastnsub = -1;	/* last queried motif size */
static int lastsol = 0;		/* the last reported solution */
static int detected;		/* the last detected solution via nsub_detected() */
static int sols[NSOLS][NCOLORS];
static int nsols;		/* the number of motifs of size lastsol */
static int sols_count[NCOLORS];	/* # solutions per motif size */
static int nsub_ref[NCOLORS];	/* # threads exploring each motif size */
static int done;

/* this makes sure each vertex is chosen as root in a single thread */
int ownroot(int nsub, int idx)
{
	int ret = 1;
	pthread_mutex_lock(&lock);
	if (nsub >= lastsol && nsub >= detected) {
		if ((nsub == lastnsub && idx > lastroot) ||
				lastnsub < 0 || nsub == lastnsub + nsub_inc) {
			ret = 0;
			lastnsub = nsub;
			lastroot = idx;
		}
	}
	pthread_mutex_unlock(&lock);
	return ret;
}

void found(int *nodes)
{
	int i = 0;
	while (nodes[i] >= 0)
		i++;
	pthread_mutex_lock(&lock);
	sols_count[i]++;
	if (lastsol < i) {
		lastsol = i;
		nsols = 0;
	}
	if (lastsol == i && nsols < NSOLS) {
		lastsol = i;
		memcpy(sols[nsols], nodes, (i + 1) * sizeof(*nodes));
		nsols++;
	}
	pthread_mutex_unlock(&lock);
}

/* the size of the optimal motifs is at least nsub */
void nsub_detected(int nsub)
{
	pthread_mutex_lock(&lock);
	if (nsub > detected && nsub <= nsub_end)
		detected = nsub;
	pthread_mutex_unlock(&lock);
}

/* return the next motif size to search for */
int nsub_next(int nsub)
{
	pthread_mutex_lock(&lock);
	if (nsub > 0) {
		nsub_ref[nsub]--;
		nsub += nsub_inc;
	} else {
		nsub = nsub_inc > 0 ? nsub_beg : nsub_end;
	}
	if (nsub < nsub_beg || nsub > nsub_end || done)
		nsub = -1;
	else
		nsub_ref[nsub]++;
	/* checking for finishing condition; no thread working on lastsol */
	if (lastsol >= nsub_beg && lastsol <= nsub_end && !nsub_ref[lastsol]) {
		/* hitting motif size limit or no motifs of size lastsol + 1 */
		if (lastsol == nsub_end || !nsub_ref[lastsol + 1])
			done = 1;
		/* nsub_detected() was not called for size nsub */
		if (dodetect && detected > lastsol &&
				!nsub_ref[detected] && detected < nsub)
			done = 1;
	}
	pthread_mutex_unlock(&lock);
	return nsub;
}

void proc_init(int beg, int end, int inc, int o, int detect)
{
	one = o;
	nsub_beg = beg;
	nsub_end = end;
	nsub_inc = inc;
	dodetect = one ? 0 : detect;
	pthread_mutex_init(&lock, NULL);
}

/* should more motifs of size nsub be enumerated? */
int nsub_finished(int nsub)
{
	/* no space for more solutions */
	if (nsub == lastsol && one)
		return 1;
	if (dodetect && nsub < detected)
		return 1;
	return nsub < lastsol || done || (nsub == nsub_end && nsols >= NSOLS);
}

int motifs(int *subs[], int size)
{
	int i;
	for (i = 0; i < nsols; i++)
		if (i < size)
			subs[i] = sols[i];
	return nsols;
}
