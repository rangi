/*
 * bl2exp, extract blast E-values from blastp output
 *
 * Copyright (C) 2012 Ali Gholami Rudi <ali at rudi dot ir>
 *
 * This program is released under the modified BSD license.
 */
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define ETHRESH		0.001

static char *readword(char *d, char *s)
{
	while (isspace(*s))
		s++;
	while (*s && !isspace(*s))
		*d++ = *s++;
	*d = '\0';
	return s;
}

static int readentries(void)
{
	char line[1024];
	char query[128];
	char other[128];
	char s_score[128];
	int junk;
	double score;
	while (1) {
		if (!fgets(line, sizeof(line), stdin))
			return 1;
		if (!strncmp(line, "Query= ", 6)) {
			readword(query, line + 7);
			break;
		}
	}
	while (1) {
		if (!fgets(line, sizeof(line), stdin))
			return 1;
		if (!strncmp(line, "Sequences producing", 16))
			break;
		if (!strncmp(line, " ***** No hits found", 16))
			return 0;
	}
	fgets(line, sizeof(line), stdin);
	while (1) {
		if (!fgets(line, sizeof(line), stdin))
			return 1;
		if (sscanf(line, "%s %d %s", other, &junk, s_score) != 3)
			break;
		sprintf(line, "%s%s", s_score[0] == 'e' ? "1" : "", s_score);
		sscanf(line, "%lf", &score);
		if (score < ETHRESH)
			printf("%s\t%s\t%lg\n", query, other, score);
	}
	return 0;
}

int main(int argc, char *argv[])
{
	if (argc > 1) {
		printf("usage: %s <blast >e_values\n", argv[0]);
		return 0;
	}
	while (!readentries())
		;
	return 0;
}
