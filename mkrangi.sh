# create RANGI input files for protein interaction networks
if [ $# != 5 ]
then
	echo "Create RANGI input files for protein interaction networks"
	echo
	echo "Usage: mkrangi.sh NET FASN FASP CMPL PRE"
	echo
	echo "  NET: the network interaction file"
	echo "  FASN: network fasta file"
	echo "  FASP: pattern fasta file"
	echo "  CMPL: pattern complexes (queries)"
	echo "  PRE: output file prefix"
	echo
	echo "Output files will be written to PRE.i.rangi, where i is"
	echo "replaced with the line number of the query in CMPL.  Note"
	echo "that you need the blast program."
	echo
	echo "The necessary files can be obtained from:"
	echo "http://igm.univ-mlv.fr/AlgoB/gramofone/"
	exit
fi

# parameters
NET="$1"
FASN="$2"
FASP="$3"
CMPL="$4"
PRE="$5"

# temporary files
FASP2=${FASP}_
QBL=${NET}_

cat $CMPL | cut -f 2 | tr ' ' '\n' | sed '/^\s\+$/d' | sort | uniq | ./fastafilt $FASP >$FASP2
formatdb -p T -i $FASN
formatdb -p T -i $FASP2
blastall -p blastp -i $FASP2 -d $FASN | ./bl2exp >$QBL

id=0
cat $CMPL | cut -f 2 | while read k
do
	id=$(( $id + 1 ))
	DAT="$PRE.$id.rangi"
	DATG="$PRE.$id.dot"
	./conv -q $QBL $k <$NET >$DAT
	./conv -g -q $QBL $k <$NET >$DATG
done

rm $FASP2 $QBL
