/*
 * RANGI, a list-colored graph motif finding program
 *
 * Copyright (C) 2012 Ali Gholami Rudi <ali at rudi dot ir>
 *
 * This program is released under the modified BSD license.
 */
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include "rangi.h"

#define MIN(a, b)	((a) < (b) ? (a) : (b))

/* N[i] and C[i] lists are terminated with a -1 */
static int N[NNODES][NNODES];	/* adjacency list */
static int V[NNODES][NNODES];	/* adjacency matrix */
static int C[NNODES][NCOLORS];	/* vertex color list */
static int D[NNODES];		/* degree of vertices */
static int S[NNODES];		/* node indices, sorted by degree */
static int F[NCOLORS]; 		/* color frequency */

static int showweights;		/* print weights too */
static int usekavosh;		/* use kavosh instead of fanmod */
static int doheur = 1;		/* enable heuristics */
static int matchthresh = 6;	/* the branching threshold for matching test */
static int verbose;		/* print the motifs as they are reported */

static int readgraph(void);
static void readdot(void);
static int maxcolors(int *nodes);
static void printmotif(int *nodes);

/* perform a matching to see if the subgraph is feasible */
static int feasible_match(int *nodes)
{
	int i = 0;
	while (nodes[i] >= 0)
		i++;
	return maxcolors(nodes) == i;
}

/* see if the vertices have at least the same number of colors */
static int feasible(int *nodes, int rem)
{
	int c[NCOLORS] = {0};
	int nc = 0;	/* nodes */
	int i, j;
	for (i = 0; nodes[i] >= 0; i++) {
		for (j = 0; C[nodes[i]][j] >= 0; j++) {
			if (!c[C[nodes[i]][j]]) {
				c[C[nodes[i]][j]] = 1;
				nc++;
			}
		}
		if (i + 1 > nc)
			return 0;
	}
	return !nsub_finished(i + rem);
}

/* find the maximum number of colors that can be mapped to nodes */
static int maxcolors(int *nodes)
{
	int *adj[MNODES] = {NULL};
	int i;
	for (i = 0; nodes[i] >= 0; i++)
		adj[i] = C[nodes[i]];
	return matching(adj, i, NCOLORS);
}

/* does adding a vertex create a larger motif? */
static int extend(int *nodes)
{
	int map[MNODES] = {0};
	int n = 0;
	int i, j, v, matched;
	while (nodes[n] >= 0)
		n++;
	for (i = 0; i < n; i++)
		map[nodes[i]] = 1;
	nodes[n + 1] = -1;
	for (i = 0; i < n; i++) {
		for (j = 0; N[nodes[i]][j] >= 0; j++) {
			v = N[nodes[i]][j];
			if (map[v])
				continue;
			map[v] = 1;
			nodes[n] = v;
			matched = maxcolors(nodes);
			nodes[n] = -1;
			if (matched == n + 1)
				return 1;
		}
	}
	return 0;
}

/* found a candidate motif */
static int subgraph(int *nodes)
{
	int n = 0;
	while (nodes[n] >= 0)
		n++;
	if (maxcolors(nodes) != n)
		return 0;
	found(nodes);
	if (verbose)
		printmotif(nodes);
	if (doheur && extend(nodes))
		nsub_detected(n + 1);
	return 1;
}

/* the number of colors with with lower frequency than c */
static int colorrank(int c)
{
	int rank = 0;
	int i;
	if (!F[c])
		return 1000;
	for (i = 0; i < NCOLORS; i++)
		if (F[i] && (F[i] < F[c] || (F[i] == F[c] && i < c)))
			rank++;
	return rank;
}

/* the total number of colors that occur in the graph */
static int ncolors(void)
{
	int n = 0;
	int i;
	for (i = 0; i < NCOLORS; i++)
		if (F[i])
			n++;
	return n;
}

/* is u a good choice for tree root? */
static int goodroot(int u, int crank)
{
	int i;
	/* ignore vertices with no edges */
	if (N[u][0] < 0)
		return 0;
	/* ignore vertices with frequent colors */
	for (i = 0; C[u][i] >= 0; i++)
		if (colorrank(C[u][i]) <= crank)
			return 1;
	return 0;
}

static int kavosh_enum(int v, int *used, int nsub);
static int fanmod_enum(int v, int *used, int nsub);
static int reachable(int v, int *used, int nsub);

/* generate all subgraphs of size nsub with all possible root vertices */
static int gensubs(int nsub)
{
	int used[NNODES] = {0};
	int i;
	int found = 0;
	int crank = ncolors() - nsub;	/* the maximum color rank of the root */
	int (*x_enum)(int, int *, int) = usekavosh ? kavosh_enum : fanmod_enum;
	for (i = 0; i < NNODES; i++) {
		if (!doheur || goodroot(S[i], crank)) {
			used[S[i]] = 1;
			if (!ownroot(nsub, i))
				if (!doheur || reachable(S[i], used, nsub))
					found += x_enum(S[i], used, nsub);
		}
	}
	return found;
}

static int degcmp(const void *i1, const void *i2)
{
	return D[*(int *) i1] - D[*(int *) i2];
}

static void init(void)
{
	int i, j;
	/* remove color-less vertices */
	for (i = 0; i < NNODES; i++)
		if (C[i][0] < 0)
			N[i][0] = -1;
	/* calculate D[i] */
	for (i = 0; i < NNODES; i++)
		for (j = 0; N[i][j] >= 0; j++)
			D[i]++;
	/* initialize S as the sorted list of vertices based on degrees */
	for (i = 0; i < NNODES; i++)
		S[i] = i;
	if (doheur)
		qsort(S, NNODES, sizeof(S[0]), degcmp);
}

static void *thread_main(void *v)
{
	int nsub = nsub_next(0);
	do {
		gensubs(nsub);
	} while ((nsub = nsub_next(nsub)) > 0);
	return NULL;
}

static void printhelp(void)
{
	printf("RANGI: a list-colored graph motif finding program\n\n");
	printf("options:\n");
	printf("\t-s size  \tsize of the motif to search for\n");
	printf("\t-n count \tthe number of threads to create\n");
	printf("\t-w       \tprint the weight of each motif too\n");
	printf("\t-d secs  \tmotif finding deadline\n");
	printf("\t-1       \tterminate after finding the first motif\n");
	printf("\t-k       \tuse kavosh instead of fanmod for enumeration\n");
	printf("\t-e       \texhaustive search; disable heuristics\n");
	printf("\t-m thresh\tminimum branches to enable matching test (%d)\n",
			matchthresh);
	printf("\t-v       \tprint the motifs as they are detected\n");
}

static void stopit(int i)
{
	fprintf(stderr, "rangi: timeout\n");
	exit(1);
}

static void printall(void);

int main(int argc, char *argv[])
{
	char *nsub_spec = NULL;
	int i;
	int one = 0;
	int nthreads = 1;
	int nsub_beg = 2;
	int nsub_end = NCOLORS;
	int nsub_dir = 1;
	for (i = 1; i < argc; i++) {
		switch (argv[i][0] == '-' ? argv[i][1] : 'h') {
		case 's':
			nsub_spec = argv[i][2] ? argv[i] + 2 : argv[++i];
			break;
		case 'n':
			nthreads = atoi(argv[i][2] ? argv[i] + 2 : argv[++i]);
			break;
		case 'd':
			alarm(atoi(argv[i][2] ? argv[i] + 2 : argv[++i]));
			break;
		case '1':
			one = 1;
			break;
		case 'w':
			showweights = 1;
			break;
		case 'k':
			usekavosh = 1;
			break;
		case 'e':
			doheur = 0;
			break;
		case 'm':
			matchthresh = atoi(argv[i][2] ? argv[i] + 2 : argv[++i]);
			break;
		case 'v':
			verbose = 1;
			break;
		default:
			printhelp();
			return 0;
		}
	}
	signal(SIGALRM, stopit);
	if (readgraph())
		readdot();
	init();
	nsub_end = ncolors();
	if (nsub_spec) {
		int c = nsub_spec[strlen(nsub_spec) - 1];
		int n = atoi(nsub_spec);
		if (c == '+')
			nsub_beg = n;
		if (c == '-')
			nsub_end = n;
		if (c != '-' && c != '+')
			nsub_beg = nsub_end = n;
		nsub_dir = c == '-' ? -1 : 1;
	}
	proc_init(nsub_beg, nsub_end, nsub_dir, one, doheur);
	/* generate subgraphs */
	if (nthreads == 1) {
		thread_main(NULL);
	} else {
		pthread_t threads[NTHREADS];
		for (i = 0; i < nthreads; i++)
			pthread_create(&threads[i], NULL, thread_main, NULL);
		for (i = 0; i < nthreads; i++)
			pthread_join(threads[i], NULL);
	}
	if (!verbose)
		printall();
	return 0;
}


/* reading the input graph */

static char names[NNODES][NLEN];	/* vertex names */
static int nnodes = 1;

static int nodeid(char *name)
{
	int i;
	for (i = 0; i < nnodes; i++)
		if (!strcmp(name, names[i]))
			return i;
	strcpy(names[nnodes], name);
	return nnodes++;
}

/*
 * Read a graph in this format:
 * + the number of vertices
 * + the name and the list of colors of each vertex, terminated with -1
 * + edges of graphs and their weights
 *
 * Example:
 * 3		# number vertices
 * v1 2 4 -1	# the name and the colors of the first vertex
 * v2 1 -1	# the name and the colors of the second vertex
 * v3 1 3 4 -1	# the name and the colors of the third vertex
 * v1 v2 102	# first edge with weight 102 between v1 and v2
 * v1 v3 223	# second edge with weight 223 between v1 and v3
 */
static int readgraph(void)
{
	int nn[NNODES] = {0};
	int i, n, c;
	char u_[NLEN], v_[NLEN];
	int u, v, w;
	if (scanf("%d", &n) != 1)
		return 1;
	for (i = 1; i <= n; i++) {
		int nc = 0;
		scanf("%s", u_);
		u = nodeid(u_);
		while (scanf("%d", &c) == 1 && c != -1) {
			if (c >= NCOLORS) {
				fprintf(stderr, "rangi: increase NCOLORS!\n");
				exit(1);
			}
			C[u][nc++] = c;
			F[c]++;
		}
		C[u][nc] = -1;
	}
	while (scanf("%s %s %d", u_, v_, &w) == 3) {
		u = nodeid(u_);
		v = nodeid(v_);
		N[u][nn[u]++] = v;
		N[v][nn[v]++] = u;
		V[u][v] = w;
		V[v][u] = w;
	}
	for (i = 0; i < NNODES; i++)
		N[i][nn[i]] = -1;
	return 0;
}

/* reads a graph in graphviz format */
static void readdot(void)
{
	int nn[NNODES] = {0};
	int u, v, w;
	long c;
	int i;
	scanf("graph G {");
	while (1) {
		if (scanf("%d", &u) != 1)
			break;
		if (getchar() == '-') {
			if (scanf("-%d[weight=\"%d\"];", &v, &w) != 2)
				break;
			N[u][nn[u]++] = v;
			N[v][nn[v]++] = u;
			V[u][v] = w;
			V[v][u] = w;
		} else {
			int nc = 0;
			if (scanf("colorlist=\"%lu\"];", &c) != 1)
				break;
			for (i = 0; i < sizeof(long) * 8; i++)
				if (c & (1ul << i)) {
					C[u][nc++] = i;
					F[i]++;
				}
			C[u][nc] = -1;
		}
	}
	for (i = 0; i < NNODES; i++)
		N[i][nn[i]] = -1;
}


/* weighing and printing motifs */

/* the minimum weight of the edges */
static int weight_min(int *nodes, int n)
{
	int w = 0x0fffffff;
	int i, j;
	for (i = 0; i < n; i++) {
		for (j = i + 1; j < n; j++) {
			int e = V[nodes[i]][nodes[j]];
			if (e > 0 && e < w)
				w = e;
		}
	}
	return w;
}

/* the sum of the weight of the edges */
static int weight_sum(int *nodes, int n)
{
	int w = 0;
	int i, j;
	for (i = 0; i < n; i++)
		for (j = i + 1; j < n; j++)
			w += V[nodes[i]][nodes[j]];
	return w;
}

/* the number of edges */
static int weight_num(int *nodes, int n)
{
	int w = 0;
	int i, j;
	for (i = 0; i < n; i++)
		for (j = i + 1; j < n; j++)
			if (V[nodes[i]][nodes[j]])
				w++;
	return w;
}

/* the weight of maximum spanning tree */
static int weight_mst(int *nodes, int n)
{
	int nei[NCOLORS][NCOLORS] = {{0}};	/* adjacency list */
	int nnei[NCOLORS] = {0};		/* number of neighbors */
	int wei[NCOLORS][NCOLORS] = {{0}};	/* weight of edges */
	int sel[NCOLORS][NCOLORS] = {{0}};	/* selected edges in mst */
	int mark[NCOLORS];			/* component of vertices */
	int w = 0;
	int oldmark, newmark;
	int i, j, u, v;
	/* create the induced subgraph of nodes */
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			wei[i][j] = V[nodes[i]][nodes[j]];
			if (wei[i][j])
				nei[i][nnei[i]++] = j;
		}
	}
	/* each node has its own component at the beginning */
	for (i = 0; i < n; i++)
		mark[i] = i;
	/* add n - 1 edges */
	for (i = 0; i < n - 1; i++) {
		int maxu = 0, maxv = 1;
		int maxedge = 0;
		/* finding the maximum edge */
		for (u = 0; u < n; u++) {
			for (j = 0; j < nnei[u]; j++) {
				v = nei[u][j];
				if (maxedge < wei[u][v] && !sel[u][v] &&
						mark[u] != mark[v]) {
					maxu = u;
					maxv = v;
				}
			}
		}
		sel[maxu][maxv] = 1;
		sel[maxv][maxu] = 1;
		/* merging the components */
		oldmark = mark[maxu];
		newmark = mark[maxv];
		for (u = 0; u < n; u++)
			if (mark[u] == oldmark)
				mark[u] = newmark;
	}
	/* calculating the weight of the spanning tree */
	for (u = 0; u < n; u++) {
		for (j = 0; j < nnei[u]; j++) {
			v = nei[u][j];
			if (u < v && sel[u][v])
				w += wei[u][v];
		}
	}
	return w;
}

/* print a motif and its weight */
static void printmotif(int *nodes)
{
	int n = 0;
	int j;
	while (nodes[n] >= 0)
		n++;
	printf("%d\t", n);
	for (j = 0; j < n; j++)
		printf("%s ", names[nodes[j]]);
	if (showweights) {
		printf("\t%d %d %d %d",
			weight_mst(nodes, n),
			weight_sum(nodes, n),
			weight_min(nodes, n),
			weight_num(nodes, n));
	}
	printf("\n");
}

static void printall(void)
{
	int *sols[NSOLS];
	int n, i;
	n = motifs(sols, NSOLS);
	for (i = 0; i < n; i++)
		printmotif(sols[i]);
}


/* the kavosh enumeration algorithm */

/* generate all C(k, n) ways of selecting k integers from {0, ..., n - 1} */
static int kavosh_comb(int k, int n, int *idx)
{
	int i, j;
	/* find the first element that could be moved right */
	for (i = k - 1; i >= 0; i--)
		if (idx[i] < n - (k - i - 1) - 1)
			break;
	if (i < 0)
		return 1;
	idx[i]++;
	for (j = i + 1; j < k; j++)
		idx[j] = idx[i] + (j - i);
	return 0;
}

/* extend the given subgraph starting at the given depth */
static int kavosh_fill(int *nodes, int *prev, int *used, int rem)
{
	int cur[NNODES];	/* vertices in this depth */
	int idx[NNODES];	/* index of selected vertices in cur[] */
	int *sel;		/* inserted vertices into nodes */
	int ncur = 0;
	int ret = 0;
	int i, j, k;
	if (doheur && !feasible(nodes, rem))
		return 0;
	if (!rem)
		return subgraph(nodes);
	/*
	 * put the vertices with the given depth into cur[] and mark
	 * them in used[]
	 */
	for (i = 0; prev[i] >= 0; i++) {
		int u = prev[i];
		for (j = 0; N[u][j] >= 0; j++) {
			int v = N[u][j];
			if (!used[v] && N[v][0] >= 0) {
				cur[ncur++] = v;
				used[v] = 1;
			}
		}
	}
	cur[ncur] = -1;
	/* where to insert new nodes */
	sel = nodes;
	while (*sel >= 0)
		sel++;
	if (doheur && ncur * (ncur - 1) / 2 >= matchthresh && !feasible_match(nodes))
		goto out;
	/* try all subgraphs with k vertices at this depth */
	for (k = 1; k <= MIN(ncur, rem); k++) {
		for (i = 0; i < k; i++)
			idx[i] = i;
		sel[k] = -1;
		/* enumerate all sel[] of size k from cur[] of size ncur */
		do {
			for (i = 0; i < k; i++)
				sel[i] = cur[idx[i]];
			ret += kavosh_fill(nodes, sel, used, rem - k);
		} while (!kavosh_comb(k, ncur, idx));
	}
out:
	for (i = 0; cur[i] >= 0; i++)
		used[cur[i]] = 0;
	*sel = -1;
	return ret;
}

/* an optimized version of the Kavosh algorithm */
static int kavosh_enum(int v, int *used, int nsub)
{
	int nodes[NNODES] = {v, -1};
	nodes[0] = v;
	return kavosh_fill(nodes, nodes, used, nsub - 1);
}


/* the fanmod enumeration algorithm */

/* insert the neighbors of w into the extension list */
static int fanmod_addext(int *sub_map, int *ext_map, int *ext, int *used, int w)
{
	int i;
	int n = 0;
	for (i = 0; N[w][i] >= 0; i++) {
		int u = N[w][i];
		if (used[u] || sub_map[u] || ext_map[u])
			continue;
		ext_map[u] = 1;
		ext[n++] = u;
	}
	ext[n] = -1;
	return n;
}

/* remove the vertices added via fanmod_addext */
static void fanmod_delext(int *ext_map, int *ext)
{
	int i;
	for (i = 0; ext[i] >= 0; i++)
		ext_map[ext[i]] = 0;
	ext[0] = -1;
}

/* recursively add vertices to sub; see the comments in fanmod_enum() */
static int fanmod_fill(int *used, int *sub, int *sub_map,
			int *ext, int *ext_map, int rem)
{
	int n_sub = 0;		/* number of entries in sub */
	int n_ext = 0;		/* number of entries in ext */
	int found = 0;
	int i;
	if (doheur && !feasible(sub, rem))
		return 0;
	if (!rem)
		return subgraph(sub);
	while (sub[n_sub] >= 0)
		n_sub++;
	while (ext[n_ext] >= 0)
		n_ext++;
	if (doheur && n_ext >= matchthresh && !feasible_match(sub))
		return 0;
	for (i = 0; i < n_ext; i++) {
		sub[n_sub] = ext[i];
		sub_map[ext[i]] = 1;
		sub[n_sub + 1] = -1;
		fanmod_addext(sub_map, ext_map, ext + n_ext, used, ext[i]);
		found += fanmod_fill(used, sub, sub_map, ext + i + 1, ext_map, rem - 1);
		fanmod_delext(ext_map, ext + n_ext);
		sub[n_sub] = -1;
		sub_map[ext[i]] = 0;
	}
	return found;
}

/* an optimized version of the ESU algorithm */
static int fanmod_enum(int v, int *used, int nsub)
{
	int sub[NNODES] = {v, -1};	/* the -1 terminated subgraph vertices */
	int sub_map[NNODES] = {0};	/* sub_map[i] is one if i is in sub */
	int ext[NNODES] = {-1};		/* the -1 terminated extension list */
	int ext_map[NNODES] = {0};	/* ext_map[i] is one if i is in ext */
	sub_map[v] = 1;
	fanmod_addext(sub_map, ext_map, ext, used, v);
	return fanmod_fill(used, sub, sub_map, ext, ext_map, nsub - 1);
}


/* count the number of reachable colors from a vertex */
static int reachable(int v, int *used, int nsub)
{
	int mark[NNODES] = {0};
	int clrs[NCOLORS] = {0};
	int q[NNODES];
	int qhead = 0;
	int qtail = 1;
	int nclrs = 0;
	int i;
	q[0] = v;
	mark[v] = 1;
	/* the BFS algorithm */
	while (qhead < qtail) {
		if (nclrs >= nsub && qtail >= nsub)
			break;
		v = q[qhead++];
		/* counting the colors in v */
		for (i = 0; C[v][i] >= 0; i++) {
			if (!clrs[C[v][i]]) {
				clrs[C[v][i]] = 1;
				nclrs++;
			}
		}
		if (mark[v] >= nsub)
			continue;
		/* adding neighbors of v to queue */
		for (i = 0; N[v][i] >= 0; i++) {
			int u = N[v][i];
			if (!mark[u] && !used[u]) {
				q[qtail++] = u;
				mark[u] = mark[v] + 1;
			}
		}
	}
	/* the neighborhood has nsub colors and the component nsub vertices */
	return nclrs >= nsub && qtail >= nsub;
}
