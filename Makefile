CC = cc
CFLAGS = -O2 -Wall
LDFLAGS = -lpthread

all: rangi conv genrq meval fastafilt bl2exp
%.o: %.c rangi.h
	$(CC) -c $(CFLAGS) $<
rangi: rangi.o mat.o proc.o
	$(CC) -o $@ $^ $(LDFLAGS)
conv: conv.o tab.o
	$(CC) -o $@ $^ $(LDFLAGS)
genrq: genrq.o
	$(CC) -o $@ $^ $(LDFLAGS)
meval: meval.o
	$(CC) -o $@ $^ $(LDFLAGS)
fastafilt: fastafilt.o
	$(CC) -o $@ $^ $(LDFLAGS)
bl2exp: bl2exp.o
	$(CC) -o $@ $^ $(LDFLAGS)
clean:
	rm -f *.o rangi conv genrq meval fastafilt bl2exp
