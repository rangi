/*
 * conv, for creating RANGI input files
 *
 * Copyright (C) 2012 Ali Gholami Rudi <ali at rudi dot ir>
 *
 * This program is released under the modified BSD license.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tab.h"

#define NLEN		64
#define NODES		20000
#define EDGES		400000
#define THRESH		0.0000001
#define NCOLORS		512

static char nodes[NODES][NLEN];		/* vertex names */
static int nnodes;
static int edges_beg[EDGES];
static int edges_end[EDGES];
static int edges_w[EDGES];		/* edge weights */
static int nedges;
static int colors[NODES][NCOLORS];
static int mapping[NODES];		/* vertex mapping in the output */
static int nmapped;

static char queries[NCOLORS][NLEN];	/* query names */
static int nqueries;

static struct tab nodes_tab;
static struct tab queries_tab;

/* find network protein */
static int id(char *s)
{
	char *r = tab_get(&nodes_tab, s);
	return r ? (r - nodes[0]) / NLEN : -1;
}

/* find protein id; insert it if not there */
static int id_def(char *s)
{
	int idx = id(s);
	if (idx < 0) {
		idx = nnodes++;
		strcpy(nodes[idx], s);
		tab_add(&nodes_tab, nodes[idx]);
	}
	return idx;
}

/* find query id */
static int qid(char *s)
{
	char *r = tab_get(&queries_tab, s);
	return r ? (r - queries[0]) / NLEN : -1;
}

static void readnetwork(FILE *fin)
{
	char s1[NLEN], s2[NLEN];
	double w;
	while (fscanf(fin, "%s %lf %s", s1, &w, s2) == 3) {
		edges_beg[nedges] = id_def(s1);
		edges_end[nedges] = id_def(s2);
		edges_w[nedges] = w * 1000000;
		nedges++;
	}
}

static void queryinteractions(char *path)
{
	char s1[NLEN], s2[NLEN];
	double p;
	FILE *qin = fopen(path, "r");
	if (!qin) {
		fprintf(stderr, "Failed to open query intraction file <%s>\n", path);
		return;
	}
	while (fscanf(qin, "%s %s %lf", s1, s2, &p) == 3) {
		if (p < THRESH) {
			if (qid(s1) >= 0 && id(s2) >= 0)
				colors[id(s2)][qid(s1)] = 1;
			if (qid(s2) >= 0 && id(s1) >= 0)
				colors[id(s1)][qid(s2)] = 1;
		}
	}
	fclose(qin);
}

/* handle queries from the same network */
static void queryself(void)
{
	int i;
	for (i = 0; i < nqueries; i++)
		if (id(queries[i]) < 0)
			fprintf(stderr, "Invalid query protein: %s\n", queries[i]);
	for (i = 0; i < nedges; i++) {
		if (qid(nodes[edges_beg[i]]) >= 0)
			colors[edges_end[i]][qid(nodes[edges_beg[i]])] = 1;
		if (qid(nodes[edges_end[i]]) >= 0)
			colors[edges_beg[i]][qid(nodes[edges_end[i]])] = 1;
	}
}

/* assign a new number to non-isolated vertices */
static void mapnodes(void)
{
	int ncolors[NODES] = {0};
	int i, j;
	for (i = 0; i < nnodes; i++)
		for (j = 0; j < NCOLORS; j++)
			if (colors[i][j])
				ncolors[i]++;
	for (i = 0; i < nnodes; i++)
		if (ncolors[i] && qid(nodes[i]) < 0)
			mapping[i] = ++nmapped;
}

static void output(void)
{
	int i, j;
	printf("%d\n", nmapped);
	for (i = 0; i < nnodes; i++) {
		if (mapping[i]) {
			printf("%s ", nodes[i]);
			for (j = 0; j < NCOLORS; j++)
				if (colors[i][j])
					printf("%d ", j);
			printf("-1\n");
		}
	}
	for (i = 0; i < nedges; i++)
		if (mapping[edges_beg[i]] && mapping[edges_end[i]])
			printf("%s %s %d\n", nodes[edges_beg[i]],
					nodes[edges_end[i]],
					edges_w[i]);
}

static void output_dot(void)
{
	int i, j;
	printf("graph G {\n");
	for (i = 0; i < nnodes; i++) {
		if (mapping[i]) {
			unsigned long c = 0;
			for (j = 0; j < NCOLORS; j++)
				if (colors[i][j])
					c |= (1ul << j);
				printf("%d[colorlist=\"%lu\"];\n",
					mapping[i], c);
		}
	}
	for (i = 0; i < nedges; i++)
		if (mapping[edges_beg[i]] && mapping[edges_end[i]])
			printf("%d--%d[weight=\"%d\"];\n",
				mapping[edges_beg[i]],
				mapping[edges_end[i]],
				edges_w[i]);
	printf("}\n");
}

static void printhelp(void)
{
	printf("usage: conv -q qfile PROT1 PROT2 ... <network >testdata\n\n");
	printf("options:\n");
	printf("\t-q qfile \tthe query blast file\n");
	printf("\t-g       \tcreate a graph in graphviz-format\n");
	exit(0);
}

int main(int argc, char *argv[])
{
	int i;
	int dot = 0;
	char *qpath = NULL;
	for (i = 1; i < argc; i++) {
		if (argv[i][0] != '-') {
			int idx = nqueries++;
			strcpy(queries[idx], argv[i]);
			if (!tab_get(&queries_tab, queries[idx]))
				tab_add(&queries_tab, queries[idx]);
		}
		if (!strcmp("-g", argv[i]))
			dot = 1;
		if (!strcmp("-q", argv[i]))
			qpath = argv[++i];
		if (!strcmp("-h", argv[i]))
			printhelp();
	}
	readnetwork(stdin);
	if (qpath)
		queryinteractions(qpath);
	else
		queryself();
	mapnodes();
	if (dot)
		output_dot();
	else
		output();
	return 0;
}
