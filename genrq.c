#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NLEN		64
#define NODES		20000
#define EDGES		400000
#define THRESH		0.0000001
#define NCOLORS		512

static char nodes[NODES][NLEN];		/* vertex names */
static int nnodes;

/* find network protein */
static int id(char *s)
{
	int i;
	for (i = 0; i < nnodes; i++)
		if (!strcmp(nodes[i], s))
			return i;
	return -1;
}

/* find protein id; insert it if not there */
static int id_def(char *s)
{
	int idx = id(s);
	if (idx < 0) {
		idx = nnodes++;
		strcpy(nodes[idx], s);
	}
	return idx;
}

static void readnetwork(FILE *fin)
{
	char s1[NLEN], s2[NLEN];
	double w;
	while (fscanf(fin, "%s %s %lf", s1, s2, &w) == 3) {
		id_def(s1);
		id_def(s2);
	}
}

static void queries(int sz)
{
	int sel[NODES] = {0};
	int idx;
	int i;
	for (i = 0; i < sz; i++) {
		do {
			idx = rand() % nnodes;
		} while (sel[idx]);
		sel[idx] = 1;
		printf("\%s\t", nodes[idx]);
	}
	printf("\n");
}

static void printhelp(void)
{
	printf("usage: genrq -s size -n count\n\n");
	printf("options:\n");
	printf("\t-s size  \tthe number of proteins in each query\n");
	printf("\t-n count \tthe number of queries to generate\n");
	exit(0);
}

int main(int argc, char *argv[])
{
	int i;
	int count = 0;
	int size = 7;
	for (i = 1; i < argc; i++) {
		if (argv[i][0] != '-' || argv[i][1] == 'h') {
			printhelp();
			exit(0);
		}
		if (argv[i][1] == 'n')
			count = atoi(argv[i][2] ? argv[i] + 2 : argv[++i]);
		if (argv[i][1] == 's')
			size = atoi(argv[i][2] ? argv[i] + 2 : argv[++i]);
	}
	readnetwork(stdin);
	for (i = 0; i < count; i++)
		queries(size);
	return 0;
}
