/*
 * fastafilt, filter fasta files based on its protein complexes
 *
 * Copyright (C) 2012 Ali Gholami Rudi <ali at rudi dot ir>
 *
 * This program is released under the modified BSD license.
 */
#include <stdio.h>
#include <string.h>

#define NPROTS		(1 << 12)
#define NLEN		(128)

static char prots[NPROTS][NLEN];
static int nprots;

static int prot_find(char *name)
{
	int i;
	for (i = 0; i < nprots; i++)
		if (!strcmp(name, prots[i]))
			return i;
	return -1;
}

static void prot_add(char *name)
{
	if (prot_find(name) >= 0)
		return;
	strcpy(prots[nprots++], name);
}

static void prot_read(FILE *fin)
{
	char name[NLEN];
	while (fscanf(fin, "%s", name) == 1)
		prot_add(name);
}

static void fasta_filt(FILE *fin)
{
	char line[256];
	char name[NLEN];
	int matched = 0;
	while (fgets(line, sizeof(line), fin)) {
		if (line[0] != '>') {
			if (matched)
				printf("%s", line);
			continue;
		}
		sscanf(line + 1, "%s", name);
		matched = prot_find(name) >= 0;
		if (matched)
			printf("%s", line);
	}
}

int main(int argc, char *argv[])
{
	FILE *fin = argc > 1 ? fopen(argv[1], "r") : NULL;
	if (!fin) {
		printf("usage: %s in.fasta <complexes >out.fasta\n", argv[0]);
		return 0;
	}
	prot_read(stdin);
	fasta_filt(fin);
	fclose(fin);
	return 0;
}
